#!/usr/bin/env node
/**
 * ruted-format: The rich Unicode text editing format
 *
 * Copyright © 2020 Huub de Beer <huub@campinecomputing.eu>
 *
 * ruted-format is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * ruted-format is distributed in the hope it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ruted-format. If not, see <https://www.gnu.org/licenses/>.
 **/
import fs from "fs";
import path from "path";
import { fileURLToPath } from 'url';

import {promisify} from "util";
import {Command} from "commander";
import {Configuration} from "../src/Configuration.js";

const NO_SUCH_FILE_OR_DIRECTORY = "ENOENT";
const stat = promisify(fs.stat);
const mkdir = promisify(fs.mkdir);
const readdir = promisify(fs.readdir);
const copyFile = promisify(fs.copyFile);
const resolve = path.resolve;
const dirname = path.dirname;

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);
const SCRIPT_DIR = resolve(__dirname);
const MODES_DIR = resolve(SCRIPT_DIR, "..", "modes");

const copyModeFiles = async function (path) {
  try {
    const modeFiles = await readdir(MODES_DIR);

    for (const modeFile of modeFiles) {
      if (modeFile.endsWith(".json")) {
        const src = `${MODES_DIR}/${modeFile}`;
        const dest = `${path}/${modeFile}`;
        try {
          await copyFile(src, dest);
        } catch (err) {
          console.warn(`Error copying mode JSON file "${src}" to the configuration directory "${dest}": `, err);
        }
      }
    }
  } catch (err) {
    console.warn(`Error trying to copy mode files to configuration directory "${path}": `, err);
  }
};

const createConfigDir = async function (path) {
  try {
    await mkdir(path, {recursive: true});
    await copyModeFiles(path);
  } catch (err) {
    console.warn(`Error creating configuration directory "${path}": `, err);
    throw err;
  }
};

const ensureValidConfigDir = async function (path) {
  try {
    const info = await stat(path);
  } catch (err) {
    if (err.code === NO_SUCH_FILE_OR_DIRECTORY) {
      try {
        await createConfigDir(path);
        // And then check it again. Will terminate because creating directory
        // succeeded.
        ensureValidConfigDir(path);
      } catch (err) {
        console.warn(`Error creating configiration directory "${path}": `, err);
      }
    } else {
      console.warn(`Unknown error trying to check configuration directory "${path}": `, err);
    }
  }
};

const showInfo = function (options) {
  const info = {
    "version": Configuration.version,
    "dir": Configuration.dir
  };

  if (options.json) {
    console.log(JSON.stringify(info));
  } else {
    console.log(`√𝚎𝚍-format ${info.version}`);
    console.log(`Configuration directory: ${info.dir}`);
    console.log("Copyright © 2020 Huub de Beer <huub@campinecomputing.eu>");
  }
};

const listModes = async function (options) {
  await ensureValidConfigDir(Configuration.dir);

  const format = await Configuration.loadFormat();
  const modes = {}
  for (const mode of format.modes()) {
    modes[mode.key] = mode.name;
  }

  if (options.json) {
    console.log(JSON.stringify(modes));
  } else {
    for (const key of Object.keys(modes)) {
      console.log(`${key}\t${modes[key]}`);
    }
  }
};

const getMode = async function(key, options) {
  await ensureValidConfigDir(Configuration.dir);

  const format = await Configuration.loadFormat();
  if (format.has(key)) {
    const mode = format.get(key);

    if (options.json) {
      console.log(mode.toJSON());
    } else {
      console.log(`Mode: ${mode.name} (${mode.key})`);
      console.log("");
      console.log(mode.description);
      console.log("");
      console.log("symbol\tsource\tname");
      for (const {symbol, source, name} of mode.rules()) {
        console.log(`${symbol}\t${source}\t${name}`);
      }
    }
  } else {
    console.warn(`There is no mode with key "${key}".`);
    process.exit(2);
  }
};

const program = new Command();

// General options
program
  .version(Configuration.version)
  ;

// Info sub command
program
  .command("info")
  .description("Show information about this ruted-format installation")
  .option("-j, --json", "use JSON as output format")
  .action(showInfo)
  ;

// List modes sub command
program
  .command("list")
  .description("List the available modes")
  .option("-j, --json", "use JSON as output format")
  .action(listModes)
  ;

// Show mode sub command
program
  .command("show <key>")
  .description("Get the mode spefied with key")
  .option("-j, --json", "use JSON as output format")
  .action(getMode)
  ;

// Run the program
program.parse(process.argv);
