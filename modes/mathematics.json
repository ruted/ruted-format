{
  "key": "m",
  "name": "mathematics",
  "description": "Various mathematical symbols, operators, and brackets.",
  "rules": [
    {
      "source": "\\forall",
      "name": "universal quantifier, or 'for all'",
      "symbol": "∀"
    },
    {
      "source": "\\exist",
      "name": "existential quantifier, or 'exist'",
      "symbol": "∃"
    },
    {
      "source": "!exist",
      "name": "negated existential quantifier, or 'not exist'",
      "symbol": "∄"
    },
    {
      "source": "\\max",
      "name": "maximum",
      "symbol": "↑"
    },
    {
      "source": "\\min",
      "name": "minimum",
      "symbol": "↓"
    },
    {
      "source": "{}",
      "name": "empty set",
      "symbol": "∅"
    },
    {
      "source": "\\split",
      "name": "split",
      "symbol": "∆"
    },
    {
      "source": "\\case",
      "name": "case",
      "symbol": "∇"
    },
    {
      "source": "\\in",
      "name": "set membership, or 'element of'",
      "symbol": "∈"
    },
    {
      "source": "!in",
      "name": "not an element of",
      "symbol": "∉"
    },
    {
      "source": "\\contains",
      "name": "contains as member",
      "symbol": "∋"
    },
    {
      "source": "!contains",
      "name": "does not contain as member",
      "symbol": "∌"
    },
    {
      "source": "\\ni",
      "name": "contains as member",
      "symbol": "∋"
    },
    {
      "source": "!ni",
      "name": "does not contain as member",
      "symbol": "∌"
    },
    {
      "source": "\\product",
      "name": "product quantifier",
      "symbol": "∏"
    },
    {
      "source": "\\sum",
      "name": "sum quantifier",
      "symbol": "∑"
    },
    {
      "source": "\\sqrt",
      "name": "square root",
      "symbol": "√"
    },
    {
      "source": "\\cbrt",
      "name": "cube root",
      "symbol": "∛"
    },
    {
      "source": "\\fort",
      "name": "fourth root",
      "symbol": "∜"
    },
    {
      "source": "\\infinity",
      "name": "infinity",
      "symbol": "∞"
    },
    {
      "source": "\\oo",
      "name": "infinity",
      "symbol": "∞"
    },
    {
      "source": "|_",
      "name": "right angle",
      "symbol": "∟"
    },
    {
      "source": "/_",
      "name": "angle",
      "symbol": "∠"
    },
    {
      "source": "\\pm",
      "name": "plus or minus",
      "symbol": "±"
    },
    {
      "source": "\\mp",
      "name": "minus or plus",
      "symbol": "∓"
    },
    {
      "source": "\\times",
      "name": "times, or product",
      "symbol": "×"
    },
    {
      "source": "\\x",
      "name": "times, or product",
      "symbol": "×"
    },
    {
      "source": "\\div",
      "name": "division",
      "symbol": "÷"
    },
    {
      "source": "\\integral",
      "name": "integral",
      "symbol": "∫"
    },
    {
      "source": "...",
      "name": "ellipsis",
      "symbol": "…"
    },
    {
      "source": "\\therefore",
      "name": "therefore",
      "symbol": "∴"
    },
    {
      "source": "\\because",
      "name": "because",
      "symbol": "∵"
    },
    {
      "source": "!",
      "name": "negation, or 'not'",
      "symbol": "¬"
    },
    {
      "source": "\\not",
      "name": "negation, or 'not'",
      "symbol": "¬"
    },
    {
      "source": "!=",
      "name": "not equals",
      "symbol": "≠"
    },
    {
      "source": "~~",
      "name": "approximately",
      "symbol": "≈"
    },
    {
      "source": "=def",
      "name": "defines",
      "symbol": "≝"
    },
    {
      "source": "===",
      "name": "equivalent",
      "symbol": "≡"
    },
    {
      "source": ".=",
      "name": "dot equals",
      "symbol": "≐"
    },
    {
      "source": "~=",
      "name": "isomorphic",
      "symbol": "≅"
    },
    {
      "source": "<=>",
      "name": "bi-implication, equivalent, 'if and only if', or 'iff'",
      "symbol": "⇔"
    },
    {
      "source": "<->",
      "name": "bi-implication, equivalent, 'if and only if', or 'iff'",
      "symbol": "↔"
    },
    {
      "source": "<<",
      "name": "smaller order",
      "symbol": "≪"
    },
    {
      "source": ">>",
      "name": "greater order",
      "symbol": "≫"
    },
    {
      "source": "<=",
      "name": "smaller or equal",
      "symbol": "≤"
    },
    {
      "source": ">=",
      "name": "larger or equal",
      "symbol": "≥"
    },
    {
      "source": "~<~",
      "name": "reduces",
      "symbol": "≺"
    },
    {
      "source": "~>~",
      "name": "reduced by",
      "symbol": "≻"
    },
    {
      "source": "<|",
      "name": "triangle left",
      "symbol": "◅"
    },
    {
      "source": "|>",
      "name": "triangle right",
      "symbol": "▻"
    },
    {
      "source": "=>",
      "name": "implies",
      "symbol": "⇒"
    },
    {
      "source": "->",
      "name": "implies, or to",
      "symbol": "→"
    },
    {
      "source": "\\superset",
      "name": "superset, or implies",
      "symbol": "⊃"
    },
    {
      "source": "\\subset",
      "name": "subset, or implied by",
      "symbol": "⊂"
    },
    {
      "source": "\\subset=",
      "name": "subset or equal",
      "symbol": "⊆"
    },
    {
      "source": "\\superset=",
      "name": "superset or equal",
      "symbol": "⊇"
    },
    {
      "source": "<-",
      "name": "from, or implied by",
      "symbol": "←"
    },
    {
      "source": "|-",
      "name": "infers",
      "symbol": "⊢"
    },
    {
      "source": "-|",
      "name": "snoc",
      "symbol": "⊣"
    },
    {
      "source": "|=",
      "name": "entails",
      "symbol": "⊧"
    },
    {
      "source": "(<",
      "name": "left angle bracket",
      "symbol": "⟨"
    },
    {
      "source": ">)",
      "name": "right angle bracket",
      "symbol": "⟩"
    },
    {
      "source": "||",
      "name": "logical or",
      "symbol": "∨"
    },
    {
      "source": "(|_",
      "name": "left floor bracket",
      "symbol": "⌊"
    },
    {
      "source": "_|)",
      "name": "right floor bracket",
      "symbol": "⌋"
    },
    {
      "source": "(-",
      "name": "left ceil bracket",
      "symbol": "⌈"
    },
    {
      "source": "-)",
      "name": "right ceil bracket",
      "symbol": "⌉"
    },
    {
      "source": "-|-",
      "name": "does not divide",
      "symbol": "∤"
    },
    {
      "source": "!|",
      "name": "does not divide",
      "symbol": "∤"
    },
    {
      "source": "//",
      "name": "parallel",
      "symbol": "∥"
    },
    {
      "source": "!//",
      "name": "not parallel",
      "symbol": "∦"
    },
    {
      "source": "=||=",
      "name": "equal and parallel",
      "symbol": "⋕"
    },
    {
      "source": "\\lightning",
      "name": "lightning",
      "symbol": "↯"
    },
    {
      "source": "(+)",
      "name": "plusl",
      "symbol": "⊕"
    },
    {
      "source": "[]",
      "name": "box",
      "symbol": "□"
    },
    {
      "source": ".\\",
      "name": "lambda",
      "symbol": "λ"
    },
    {
      "source": "\\const",
      "name": "const",
      "symbol": "•"
    },
    {
      "source": "BB",
      "name": "Booleans",
      "symbol": "𝔹"
    },
    {
      "source": "CC",
      "name": "complex numbers",
      "symbol": "ℂ"
    },
    {
      "source": "\\partial",
      "name": "partial",
      "symbol": "∂"
    },
    {
      "source": "EE",
      "name": "expected value",
      "symbol": "𝔼"
    },
    {
      "source": "FF",
      "name": "Galois field",
      "symbol": "𝔽"
    },
    {
      "source": "HH",
      "name": "Hamiltonian numbers",
      "symbol": "ℍ"
    },
    {
      "source": "II",
      "name": "indicator of",
      "symbol": "𝕀"
    },
    {
      "source": "NN",
      "name": "natural numbers",
      "symbol": "ℕ"
    },
    {
      "source": "LL",
      "name": "list of",
      "symbol": "𝕃"
    },
    {
      "source": "(.)",
      "name": "dotl",
      "symbol": "⊙"
    },
    {
      "source": "PP",
      "name": "prime numbers",
      "symbol": "ℙ"
    },
    {
      "source": "QQ",
      "name": "rational numbers",
      "symbol": "ℚ"
    },
    {
      "source": "RR",
      "name": "real numbers",
      "symbol": "ℝ"
    },
    {
      "source": "\\dagger",
      "name": "dagger",
      "symbol": "†"
    },
    {
      "source": "\\top",
      "name": "top element",
      "symbol": "⊤"
    },
    {
      "source": "\\bottom",
      "name": "bottom element",
      "symbol": "⊥"
    },
    {
      "source": "_|_",
      "name": "bottom element",
      "symbol": "⊥"
    },
    {
      "source": "UU",
      "name": "universal set",
      "symbol": "𝕌"
    },
    {
      "source": "\\union",
      "name": "union",
      "symbol": "∪"
    },
    {
      "source": "\\intersect",
      "name": "intersection",
      "symbol": "∩"
    },
    {
      "source": "\\/",
      "name": "or",
      "symbol": "∨"
    },
    {
      "source": "/\\",
      "name": "and",
      "symbol": "∧"
    },
    {
      "source": "&&",
      "name": "and",
      "symbol": "∧"
    },
    {
      "source": "(*)",
      "name": "timesl",
      "symbol": "⊗"
    },
    {
      "source": "|><",
      "name": "left times",
      "symbol": "⋉"
    },
    {
      "source": "><|",
      "name": "right times",
      "symbol": "⋊"
    },
    {
      "source": "|><|",
      "name": "natural join",
      "symbol": "⋈"
    },
    {
      "source": "ZZ",
      "name": "integer numbers",
      "symbol": "ℤ"
    },
    {
      "source": "(-)",
      "name": "minl",
      "symbol": "⊖"
    },
    {
      "source": "\\pi",
      "name": "pi",
      "symbol": "π"
    },
    {
      "source": "(=)",
      "name": "isl",
      "symbol": "⊜"
    },
    {
      "source": "[x]",
      "name": "crossed box",
      "symbol": "⊠"
    },
    {
      "source": "[.]",
      "name": "dotted box",
      "symbol": "⊡"
    },
    {
      "source": "~~~",
      "name": "congruent",
      "symbol": "≋"
    },
    {
      "source": "\\integral2",
      "name": "double integral",
      "symbol": "∬"
    },
    {
      "source": "\\integral3",
      "name": "tripple integral",
      "symbol": "∭"
    },
    {
      "source": ">]",
      "name": "greater",
      "symbol": "⊐"
    },
    {
      "source": "[<",
      "name": "smaller",
      "symbol": "⊏"
    },
    {
      "source": ">]=",
      "name": "greater or equal",
      "symbol": "⊒"
    },
    {
      "source": "[<=",
      "name": "smaller or equal",
      "symbol": "⊑"
    },
    {
      "source": "|==",
      "name": "entails, or models",
      "symbol": "⊨"
    },
    {
      "source": "||-",
      "name": "congruent",
      "symbol": "⊩"
    },
    {
      "source": "<<<",
      "name": "to the left",
      "symbol": "⋘"
    },
    {
      "source": ">>>",
      "name": "to the right",
      "symbol": "⋙"
    },
    {
      "source": "\\integral4",
      "name": "quadruple integral",
      "symbol": "⨌"
    },
    {
      "source": "(|",
      "name": "left banana bracket",
      "symbol": "⦇"
    },
    {
      "source": "|)",
      "name": "right banana bracket",
      "symbol": "⦈"
    },
    {
      "source": "(<)",
      "name": "lesserthanl",
      "symbol": "⧀"
    },
    {
      "source": "(>)",
      "name": "greaterthanl",
      "symbol": "⧁"
    },
    {
      "source": "(|)",
      "name": "barl",
      "symbol": "⦶"
    },
    {
      "source": "[(",
      "name": "left anamorphism bracket",
      "symbol": "〖"
    },
    {
      "source": ")]",
      "name": "right anamorphism bracket",
      "symbol": "〗"
    },
    {
      "source": "\\blackcase",
      "name": "coinductive case",
      "symbol": "▼"
    },
    {
      "source": "^A",
      "name": "superscript A",
      "symbol": "ᴬ"
    },
    {
      "source": "^B",
      "name": "superscript B",
      "symbol": "ᴮ"
    },
    {
      "source": "^D",
      "name": "superscript D",
      "symbol": "ᴰ"
    },
    {
      "source": "^E",
      "name": "superscript E",
      "symbol": "ᴱ"
    },
    {
      "source": "^G",
      "name": "superscript G",
      "symbol": "ᴳ"
    },
    {
      "source": "^H",
      "name": "superscript H",
      "symbol": "ᴴ"
    },
    {
      "source": "^I",
      "name": "superscript I",
      "symbol": "ᴵ"
    },
    {
      "source": "^J",
      "name": "superscript J",
      "symbol": "ᴶ"
    },
    {
      "source": "^K",
      "name": "superscript K",
      "symbol": "ᴷ"
    },
    {
      "source": "^L",
      "name": "superscript L",
      "symbol": "ᴸ"
    },
    {
      "source": "^M",
      "name": "superscript M",
      "symbol": "ᴹ"
    },
    {
      "source": "^N",
      "name": "superscript N",
      "symbol": "ᴺ"
    },
    {
      "source": "^O",
      "name": "superscript O",
      "symbol": "ᴼ"
    },
    {
      "source": "^P",
      "name": "superscript P",
      "symbol": "ᴾ"
    },
    {
      "source": "^R",
      "name": "superscript R",
      "symbol": "ᴿ"
    },
    {
      "source": "^T",
      "name": "superscript T",
      "symbol": "ᵀ"
    },
    {
      "source": "^U",
      "name": "superscript U",
      "symbol": "ᵁ"
    },
    {
      "source": "^V",
      "name": "superscript V",
      "symbol": "ⱽ"
    },
    {
      "source": "^W",
      "name": "superscript W",
      "symbol": "ᵂ"
    },
    {
      "source": "^a",
      "name": "superscript a",
      "symbol": "ᵃ"
    },
    {
      "source": "^b",
      "name": "superscript b",
      "symbol": "ᵇ"
    },
    {
      "source": "^c",
      "name": "superscript c",
      "symbol": "ᶜ"
    },
    {
      "source": "^d",
      "name": "superscript d",
      "symbol": "ᵈ"
    },
    {
      "source": "^e",
      "name": "superscript e",
      "symbol": "ᵉ"
    },
    {
      "source": "^f",
      "name": "superscript f",
      "symbol": "ᶠ"
    },
    {
      "source": "^g",
      "name": "superscript g",
      "symbol": "ᵍ"
    },
    {
      "source": "^h",
      "name": "superscript h",
      "symbol": "ʰ"
    },
    {
      "source": "^i",
      "name": "superscript i",
      "symbol": "ⁱ"
    },
    {
      "source": "^j",
      "name": "superscript j",
      "symbol": "ʲ"
    },
    {
      "source": "^k",
      "name": "superscript k",
      "symbol": "ᵏ"
    },
    {
      "source": "^l",
      "name": "superscript l",
      "symbol": "ˡ"
    },
    {
      "source": "^m",
      "name": "superscript m",
      "symbol": "ᵐ"
    },
    {
      "source": "^n",
      "name": "superscript n",
      "symbol": "ⁿ"
    },
    {
      "source": "^o",
      "name": "superscript o",
      "symbol": "ᵒ"
    },
    {
      "source": "^p",
      "name": "superscript p",
      "symbol": "ᵖ"
    },
    {
      "source": "^r",
      "name": "superscript r",
      "symbol": "ʳ"
    },
    {
      "source": "^s",
      "name": "superscript s",
      "symbol": "ˢ"
    },
    {
      "source": "^t",
      "name": "superscript t",
      "symbol": "ᵗ"
    },
    {
      "source": "^u",
      "name": "superscript u",
      "symbol": "ᵘ"
    },
    {
      "source": "^v",
      "name": "superscript v",
      "symbol": "ᵛ"
    },
    {
      "source": "^w",
      "name": "superscript w",
      "symbol": "ʷ"
    },
    {
      "source": "^x",
      "name": "superscript x",
      "symbol": "ˣ"
    },
    {
      "source": "^y",
      "name": "superscript y",
      "symbol": "ʸ"
    },
    {
      "source": "^z",
      "name": "superscript z",
      "symbol": "ᶻ"
    },
    {
      "source": "^4",
      "name": "superscript 4",
      "symbol": "⁴"
    },
    {
      "source": "^5",
      "name": "superscript 5",
      "symbol": "⁵"
    },
    {
      "source": "^6",
      "name": "superscript 6",
      "symbol": "⁶"
    },
    {
      "source": "^7",
      "name": "superscript 7",
      "symbol": "⁷"
    },
    {
      "source": "^8",
      "name": "superscript 8",
      "symbol": "⁸"
    },
    {
      "source": "^9",
      "name": "superscript 9",
      "symbol": "⁹"
    },
    {
      "source": "^+",
      "name": "superscript +",
      "symbol": "⁺"
    },
    {
      "source": "^-",
      "name": "superscript -",
      "symbol": "⁻"
    },
    {
      "source": "^=",
      "name": "superscript =",
      "symbol": "⁼"
    },
    {
      "source": "^(",
      "name": "superscript (",
      "symbol": "⁽"
    },
    {
      "source": "^)",
      "name": "superscript )",
      "symbol": "⁾"
    },
    {
      "source": "^0",
      "name": "superscript 0",
      "symbol": "⁰"
    },{
      "source": "^1",
      "name": "superscript 1",
      "symbol": "¹"
    },{
      "source": "^2",
      "name": "superscript 2",
      "symbol": "²"
    },{
      "source": "^3",
      "name": "superscript 3",
      "symbol": "³"
    },
    {
      "source": "^.",
      "name": "superscript ",
      "symbol": "˙"
    },
    {
      "source": "^*",
      "name": "superscript *",
      "symbol": "ᕁ"
    },
    {
      "source": ";o",
      "name": "compose",
      "symbol": "∘"
    },
    {
      "source": "::",
      "name": "proportionality",
      "symbol": "∷"
    },
    {
      "source": "_a",
      "name": "subscript a",
      "symbol": "ₐ"
    },
    {
      "source": "_e",
      "name": "subscript e",
      "symbol": "ₑ"
    },
    {
      "source": "_h",
      "name": "subscript h",
      "symbol": "ₕ"
    },
    {
      "source": "_i",
      "name": "subscript i",
      "symbol": "ᵢ"
    },
    {
      "source": "_j",
      "name": "subscript j",
      "symbol": "ⱼ"
    },
    {
      "source": "_k",
      "name": "subscript k",
      "symbol": "ₖ"
    },
    {
      "source": "_l",
      "name": "subscript l",
      "symbol": "ₗ"
    },
    {
      "source": "_m",
      "name": "subscript m",
      "symbol": "ₘ"
    },
    {
      "source": "_n",
      "name": "subscript n",
      "symbol": "ₙ"
    },
    {
      "source": "_o",
      "name": "subscript o",
      "symbol": "ₒ"
    },
    {
      "source": "_p",
      "name": "subscript p",
      "symbol": "ₚ"
    },
    {
      "source": "_r",
      "name": "subscript r",
      "symbol": "ᵣ"
    },
    {
      "source": "_s",
      "name": "subscript s",
      "symbol": "ₛ"
    },
    {
      "source": "_t",
      "name": "subscript t",
      "symbol": "ₜ"
    },
    {
      "source": "_u",
      "name": "subscript u",
      "symbol": "ᵤ"
    },
    {
      "source": "_v",
      "name": "subscript v",
      "symbol": "ᵥ"
    },
    {
      "source": "_x",
      "name": "subscript x",
      "symbol": "ₓ"
    }, 		
    {
      "source": "_0",
      "name": "subscript 0",
      "symbol": "₀"
    },
    {
      "source": "_1",
      "name": "subscript 1",
      "symbol": "₁"
    },
    {
      "source": "_2",
      "name": "subscript 2",
      "symbol": "₂"
    },
    {
      "source": "_3",
      "name": "subscript 3",
      "symbol": "₃"
    },
    {
      "source": "_4",
      "name": "subscript 4",
      "symbol": "₄"
    },
    {
      "source": "_5",
      "name": "subscript 5",
      "symbol": "₅"
    },
    {
      "source": "_6",
      "name": "subscript 6",
      "symbol": "₆"
    },
    {
      "source": "_7",
      "name": "subscript 7",
      "symbol": "₇"
    },
    {
      "source": "_8",
      "name": "subscript 8",
      "symbol": "₈"
    },
    {
      "source": "_9",
      "name": "subscript 9",
      "symbol": "₉"
    },
    {
      "source": "_+",
      "name": "subscript +",
      "symbol": "₊"
    },
    {
      "source": "_-",
      "name": "subscript -",
      "symbol": "₋"
    },
    {
      "source": "_=",
      "name": "subscript =",
      "symbol": "₌"
    },
    {
      "source": "_(",
      "name": "subscript (",
      "symbol": "₍"
    },
    {
      "source": "_)",
      "name": "subscript )",
      "symbol": "₎"
    }, 
    {
      "source": "_<-",
      "name": "subscript left arrow",
      "symbol": "˿"
    },
    {
      "source": "<.",
      "name": "left dotted angle bracket",
      "symbol": "⦑"
    },
    {
      "source": ".>",
      "name": "right dotted angle bracket",
      "symbol": "⦒"
    },
    {
      "source": ":=",
      "name": "becomes, or definition",
      "symbol": "≔"
    },
    {
      "source": "\\nand",
      "name": "nand",
      "symbol": "⊼"
    },
    {
      "source": "\\nor",
      "name": "nor",
      "symbol": "⊽"
    },
    {
      "source": "\\xor",
      "name": "xor",
      "symbol": "⊻"
    },
    {
      "source": "\\injl",
      "name": "left injection",
      "symbol": "↪"
    },
    {
      "source": "***",
      "name": "asterism",
      "symbol": "⁂"
    },
    {
      "source": "[[",
      "name": "left open square bracket",
      "symbol": "〚"
    },
    {
      "source": "]]",
      "name": "right open square bracket",
      "symbol": "〛"
    },
    {
      "source": "\\qed",
      "name": "end of proof",
      "symbol": "∎"
    },
    {
      "source": "\\alpha",
      "name": "Greek letter alpha",
      "symbol": "α"
    },
    {
      "source": "\\beta",
      "name": "Greek letter beta",
      "symbol": "β"
    },
    {
      "source": "\\gamma",
      "name": "Greek letter gamma",
      "symbol": "γ"
    },
    {
      "source": "\\delta",
      "name": "Greek letter delta",
      "symbol": "δ"
    },
    {
      "source": "\\epsilon",
      "name": "Greek letter epsilon",
      "symbol": "ε"
    },
    {
      "source": "\\phi",
      "name": "Greek letter phi",
      "symbol": "φ"
    },
    {
      "source": "\\omega",
      "name": "Greek letter omega",
      "symbol": "ω"
    },
    {
      "source": "\\lambda",
      "name": "Greek letter lambda",
      "symbol": "λ"
    },
    {
      "source": "\\mu",
      "name": "Greek letter mu",
      "symbol": "μ"
    },
    {
      "source": "\\sigma",
      "name": "Greek letter sigma",
      "symbol": "σ"
    },
    {
      "source": "\\injr",
      "name": "right injection",
      "symbol": "↩"
    }
  ]
}
