# √𝚎𝚍-format: The rich Unicode text editing format

All tools in √𝚎𝚍, the rich Unicode text editing (RUTED, pronounced ˈruːtɪd)
suite, share the same underlying format. The √𝚎𝚍 suite allows you to enter
Unicode symbols easily and consistently in different applications and
operating systems. √𝚎𝚍-format consists of various detailed modes that map
source input strings to symbolic Unicode characters. Each mode focuses on a
single category of Unicode symbols, like characters in a bold font,
mathematical symbols, arrows, etc. 

I've not developed √𝚎𝚍-format much in the past years, so you can consider the
current version stable. Current version: 1.0.1.

## Installation

Install via NPM:

```bash
npm install -g ruted-format
```

## Usage

Ruted-format is meant to be used as a back-end for all kinds of editors. Query
ruted-format to get a list of the available modes or to get a mode's list of
replacement rules to apply in your editor.

### Use in other programs

Currently, there is just one project using ruted-format to offer easily
entering unicode symbols: [ruted-vim](https://gitlab.com/ruted/ruted-vim).

### Command-line usage


```
Usage: ruted-format [options] [command]

Options:
-V, --version         output the version number
-h, --help            display help for command

Commands:
info [options]        Show information about this ruted-format installation
list [options]        List the available modes
show [options] <key>  Get the mode spefied with key
help [command]        display help for command
```
