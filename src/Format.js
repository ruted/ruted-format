/**
 * ruted-format: The rich Unicode text editing format
 *
 * Copyright © 2020 Huub de Beer <huub@campinecomputing.eu>
 *
 * ruted-format is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * ruted-format is distributed in the hope it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ruted-format. If not, see <https://www.gnu.org/licenses/>.
 **/
import {Configuration} from "./Configuration.js";
import {Keyboard} from "./Keyboard.js";
import {RutedError} from "./RutedError.js";

const FormatError = class extends RutedError {};

const DuplicateModeFormatError = class extends FormatError {
  constructor(mode) {
    super(`This format already has a mode with key "${mode.key}".`);
  }
};

const IllegalModeKeyFormatError = class extends FormatError {
  constructor(invalidKey) {
    super(`A mode's key should be exactly one single typeable character long. Got "${invalidKey}" instead.`);
  }
};

const ModeNotFoundFormatError = class extends FormatError {
  constructor(key) {
    super(`No mode with key "${key}" does exists in this format.`);
  }
};


/**
 * Ruted Format comprises a valid set of {@link Mode}s
 **/
const Format = class {
  #modes = new Map()

  /**
   * Create a new Format.
   **/
  constructor() {
  }

  /**
   * Iterate over all the modes in this Format.
   *
   * @yields {Mode}
   **/
  * modes() {
    for (const mode of this.#modes.values()) {
      yield mode;
    }
  }

  /**
   * Add a new mode to this Format.
   *
   * @param {Mode} mode - The mode to add to this Format.
   *
   * @throws {DuplicateModeFormatError} A mode's key should be unique in a format.
   **/
  add(mode) {
    if (this.has(mode.key)) {
      throw new DuplicateModeFormatError(mode);
    }
    
    this.#modes.set(mode.key, mode);
  }

  /**
   * Get a mode from this format given the mode's key. Use {@link Format#find} to search for a mode.
   *
   * @param {String} key - The mode's key.
   *
   * @return {Mode} Returns the mode with the key.
   *
   * @throws {IllegalModeKeyFormatError} A mode's key should be exactly one
   * single typeable character.
   *
   * @throws {ModeNotFoundFormatError} This format does not have a mode with
   * the supplied key.
   **/
  get(key) {
    if (this.has(key)) {
      return this.#modes.get(key);
    } else {
      throw new ModeNotFoundFormatError(key);
    }
  }

  /**
   * Does this format have a mode with key?
   *
   * @param {String} key - The mode's key to check.
   *
   * @return {Boolean} Wether this Format contains a mode with key or not.
   *
   * @throws {IllegalModeKeyFormatError} A mode's key should be exactly one
   * single typeable character.
   **/
  has(key) {
    if (key.length != 1 || !Keyboard.canBeTyped(key)) {
      throw new IllegalModeKeyFormatError(key);
    }

    return this.#modes.has(key);
  }

  /**
   * Remove a mode from this format.
   *
   * @param {String} key - The key of the mode to remove from this format.
   *
   * @throws {IllegalModeKeyFormatError} A mode's key should be exactly one
   * single typeable character.
   *
   * @throws {ModeNotFoundFormatError} This format does not have a mode with
   * the supplied key.
   **/
  remove(key) {
    if (this.has(key)) {
      return this.#modes.delete(key);
    } else {
      throw new ModeNotFoundFormatError(key);
    }
  }
};

export {
  Format,
  DuplicateModeFormatError,
  IllegalModeKeyFormatError,
  ModeNotFoundFormatError,
};
