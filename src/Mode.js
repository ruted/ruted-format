/**
 * ruted-format: The rich Unicode text editing format
 *
 * Copyright © 2020 Huub de Beer <huub@campinecomputing.eu>
 *
 * ruted-format is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * ruted-format is distributed in the hope it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ruted-format. If not, see <https://www.gnu.org/licenses/>.
 **/

import {Converter} from "./Converter.js";
import {Keyboard} from "./Keyboard.js";
import {RutedError} from "./RutedError.js";

/**
 * A replacement rule
 * @typedef {Object} Rule
 * @property {String} Rule.name - The rule's name
 * @property {String} Rule.source - The rule's source string representing a
 * Unicode symbol
 * @property {String} Rule.symbol - The rule's Unicode symbol represented by
 * the source
 **/

/**
 * A mode's field names.
 * @typedef {("key"|"name"|"description")} ModeField
 **/
const MODE_KEY = "key";
const MODE_NAME = "name";
const MODE_DESCRIPTION = "description";

/**
 * Errors thrown when creating or using a mode.
 **/
const ModeError = class extends RutedError {

  /**
   * Create a new ModeError.
   *
   * @param {String} msg - The error message.
   **/
  constructor(msg) {
    super(msg);
  }
};

/**
 * Mode error thrown when one of the documentation fields key, name, or
 * description are empty.
 **/
const EmptyDocumentationFieldModeError = class extends ModeError {
  #field

  /**
   * Create a new EmptyDocumentationFieldModeError.
   *
   * @param {ModeField} field - The name of the field that is empty.
   **/
  constructor(field) {
    super(`A mode's ${field} cannot be the empty string.`);
    this.#field = field;
  }

  /**
   * Get the name of the field that is empty.
   *
   * @return {ModeField}
   **/
  get field() {
    return this.#field;
  }
};

/**
 * Errors thrown for issues with a mode's key.
 **/
const KeyModeError = class extends ModeError {
  #key

  /**
   * Create a new KeyModeError.
   *
   * @param {String} key - The erronous key.
   * @param {String} msg - The error message.
   **/
  constructor(key, msg) {
    super(msg);
    this.#key = key;
  }

  /**
   * The erroneous key.
   *
   * @returns {String}
   **/
  get key() {
    return this.#key;
  }
};

/**
 * Key mode error thrown when the key is not exactly one character.
 **/
const NonSingletonKeyModeError = class extends KeyModeError {
  constructor(key) {
    super(
      `Expected a mode's key to be exactly 1 character; Found "${key}" instead, which has ${key.length} characters.`
    );
  }
};

/**
 * Key mode error thrown when the key is not typeable by the keyboard. Because
 * this key is used to activate a mode via the keyboard, non typeable keys are
 * problematic.
 **/
const NonTypeableKeyModeError = class extends ModeError {
  constructor(key) {
    super(
      `Expected a mode's key that can be entered by keyboard; Found "${key}" instead.`
    );
  }
};

/**
 * Mode error thrown when a replacement rule to be added does not have a name.
 **/
const EmptyNameRuleModeError = class extends ModeError {
  constructor() {
    super("Expected a non empty name for any replacement rule to add");
  }
};

/**
 * Mode error thrown when a rule cannot be found.
 **/
const UnknownRuleModeError = class extends ModeError {
  #rule

  /**
   * Create a new UnknownRuleModeError.
   *
   * @param {Rule} rule - The erroenous rule.
   **/
  constructor(rule) {
    super(`The rule '${JSON.stringify(rule)}' does not exist in this mode. Make sure all properties of the rule are correct.`);
    this.#rule = rule;
  }

  /**
   * Get the erroneous rule.
   *
   * @return {Rule}
   **/
  get rule() {
    return this.#rule;
  }
}

/**
 * A mode represents a documented set of replacement rules. Each mode has a
 * single character key, a simple name, and a longer description documenting
 * the mode. Furthermore, the mode contains zero or more replacement rules.
 *
 **/
const Mode = class {
  #key
  #name
  #description
  #rules
  #converter

  /**
   * Create a new mode from a JSON representation of a mode.
   *
   * @param {String} json - The JSON representation to create a new mode from.
   *
   * @returns {Mode}
   *
   * @throws {SyntaxError} if the JSON string is not valid JSON
   *
   * @throws {EmptyDocumentationFieldModeError} A mode's key, name and
   * description are required and cannot be the empty string.
   *
   * @throws {NonSingletonKeyModeError} A mode's key should be exactly one
   * character long.
   *
   * @throws {NonTypeableKeyModeError} A mode's key should be typeable by the
   * keyboard because this key is used to activate a mode via the keyboard.
   *
   * @throws {EmptyNameRuleModeError} A replacement rule should have a
   * non-empty name.
   *
   * @throws {ReplacementRuleError} See {@link Converter#addReplacementRule}
   * for possible errors thrown when trying to add an invalid rule in the
   * context of the current state of this mode.
   **/
  static fromJSON(json) {
    const config = JSON.parse(json);
    const mode = new Mode(config.key || "", config.name || "", config.description || "");
    for (const rule of config.rules) {
      mode.addRule(rule.name || "", rule.source || "", rule.symbol || "");
    }
    return mode;
  }

  /**
   * Create a new mode.
   *
   * @param {String} key - a single character name for the mode. The key
   * should be a typeable by the keyboard because it is used to activate a
   * mode via the keyboard.
   * @param {String} name - a short descriptive name for the mode
   * @param {String} description - a longer description for the mode
   *
   * @throws {EmptyDocumentationFieldModeError} A mode's key, name and
   * description are required and cannot be the empty string.
   *
   * @throws {NonSingletonKeyModeError} A mode's key should be exactly one
   * character long.
   *
   * @throws {NonTypeableKeyModeError} A mode's key should be typeable by the
   * keyboard because this key is used to activate a mode via the keyboard.
   *
   **/
  constructor(key, name, description) {
    if ("" === key) {
      throw new EmptyDocumentationFieldModeError(MODE_KEY);
    }

    if (key.length != 1) {
      throw new NonSingletonKeyModeError(key);
    }

    if (!Keyboard.canBeTyped(key)) {
      throw new NonTypeableKeyModeError(key);
    }

    this.#key = key;

    if ("" === name) {
      throw new EmptyDocumentationFieldModeError(MODE_NAME);
    }

    this.#name = name;

    if ("" === description) {
      throw new EmptyDocumentationFieldModeError(MODE_DESCRIPTION);
    }

    this.#description = description;

    this.#rules = [];
    this.#converter = new Converter();
  }

  /**
   * The key of this mode.
   *
   * @return {String}
   **/
  get key() {
    return this.#key;
  }

  /**
   * The name of this mode.
   *
   * @return {String}
   **/
  get name() {
    return this.#name;
  }

  /**
   * A description of this mode.
   *
   * @return {String}
   **/
  get description() {
    return this.#description;
  }

  /**
   * A description of this mode.
   *
   * @param {String} newDescription - A new description for this mode
   *
   * @throws {EmptyDocumentationFieldModeError} A description cannot be empty
   **/
  set description(newDescription) {
    if ("" === newDescription) {
      throw new EmptyDocumentationFieldModeError(MODE_DESCRIPTION);
    }

    this.#description = newDescription;
  }

  /**
   * Iterate over all the replacement rules in this mode.
   *
   * @yields {Rule}
   **/
  * rules() {
    for (const rule of this.#rules) {
      yield rule;
    }
  }

  /**
   * Add a new replacement rule to this mode.
   *
   * @param {String} name - A non-empty name describing the replacement rule
   * to add.
   * @param {String} source - A non-empty string of typeable characters
   * reprecenting a Unicode symbol to be replaced by that Unicode sumbol.
   * @param {String} symbol - A single Unicode character as replacement for
   * the source.
   *
   * @throws {EmptyNameRuleModeError} A replacement rule should have a
   * non-empty name.
   *
   * @throws {ReplacementRuleError} See {@link Converter#addReplacementRule}
   * for possible errors thrown when trying to add an invalid rule in the
   * context of the current state of this mode.
   **/
  addRule(name, source, symbol) {
    if ("" === name) {
      throw new EmptyNameRuleModeError();
    }

    this.#converter.addReplacementRule(source, symbol); // can throw ReplacementRuleError
    this.#rules.push({name, source, symbol});
  }

  /**
   * Find the index of the given rule. The rule's properties should match
   * exactly.
   *
   * @param {Rule} rule - The rule to search for.
   * @returns {Number} The index of rule in this mode, or, if it cannot be
   * found, -1.
   **/
  #findRuleIndex(rule) {
    return this.#rules.findIndex(
      ({name, source, symbol}) => name === rule.name && source === rule.source && symbol === rule.symbol
    );
  }

  /**
   * Remove a rule from this mode.
   *
   * @param {Rule} rule - The rule to remove
   **/
  removeRule(rule) {
    const index = this.#findRuleIndex(rule);

    if (index >= 0) {
      this.#rules.splice(index, 1);
      this.#converter = new Converter(this.#rules); // Will not throw any error because removing a rule will not invalidate the set of replacement rules.
    }
  }

  /**
   * Update a rule's name. 
   *
   * Note. The name is the only property of a rule that can be
   * updated. For other updates, remove the rule first and then add it anew
   * with the changes applied.
   *
   * @param {Rule} rule - the rule to update
   * @param {String} newName - the rule's new name
   *
   * @throws {EmptyNameRuleModeError} The name of a rule cannot be empty.
   * @throws {UnknownRuleModeError} The rule to update should be in this mode.
   **/
  updateRuleName(rule, newName) {
    if ("" === newName) {
      throw new EmptyNameRuleModeError();
    }

    const index = this.#findRuleIndex(rule);

    if (index >= 0) {
      this.#rules[index].name = newName
    } else {
      throw new UnknownRuleModeError(rule);
    }
  }

  /**
   * Convert an input string according to this mode's replacement rules.
   * 
   * @param {String} input - The input string to convert
   *
   * @returns {String} The input string with all replacement rules of this
   * mode applied.
   **/
  convert(input) {
    return this.#converter.convert(input);
  }

  /**
   * Convert this mode to a JSON string representation.
   *
   * @return {String} A JSON representation of this mode.
   **/
  toJSON() {
    return JSON.stringify({
      key: this.key,
      name: this.name,
      description: this.description,
      rules: this.#rules
    });
  }

};

export {
  Mode,
  ModeError,
  EmptyNameRuleModeError,
  EmptyDocumentationFieldModeError,
  NonSingletonKeyModeError,
  NonTypeableKeyModeError,
  UnknownRuleModeError
};
