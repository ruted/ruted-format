/**
 * ruted-format: The rich Unicode text editing format
 *
 * Copyright © 2020 Huub de Beer <huub@campinecomputing.eu>
 *
 * ruted-format is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * ruted-format is distributed in the hope it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ruted-format. If not, see <https://www.gnu.org/licenses/>.
 **/
import fs from "fs";
import path from "path";
import {promisify} from "util";

import envPaths from "env-paths";

import {Format} from "./Format.js";
import {Mode} from "./Mode.js";
import {RutedError} from "./RutedError.js";

const readdir = promisify(fs.readdir);
const readFile = promisify(fs.readFile);
const resolve = path.resolve;

const ConfigDirError = class extends RutedError {
  constructor(dir, err) {
    console.warn(err);
    super(`Error reading configuration directory "${dir}".`, err);
  }
};

const ModeFileIOError = class extends RutedError {
  constructor(file, err) {
    super(`Error reading the JSON file "${file}".`, err);
  }
};

const paths = envPaths("ruted-format", {
  suffix: ""
});

const modes = async function * (dir) {
  try {
    const modeFiles = await readdir(dir);
    for (const file of modeFiles) {
      if (file.endsWith(".json")) {
        try {
          const json = await readFile(resolve(dir, file), "utf8");
          yield Mode.fromJSON(json);
        } catch (err) {
          throw new ModeFileIOError(file, err);
        }
      }
    }
  } catch (err) {
    throw new ConfigDirError(dir, err);
  }
};

/**
 * Configuration of ruted-format.
 **/
const _Configuration = class {
  #dir

  constructor(dir = paths.data) {
    this.#dir = dir;
  }

  /**
   * Get the directory where the mode files are stored.
   *
   * @return {String}
   **/
  get dir() {
    return this.#dir;
  }

  /**
   * Load a format from a directory. Defaults to the configuration directory.
   *
   * @param {String} [dir = Configuration.dir] - The directory containing the
   * format mode's JSON files
   *
   * @return {Format} 
   *
   * @throws {ModeFileError} Thrown when reading a mode's JSON file
   * from dir fails.
   *
   * @throws {ConfigDirError} Thrown when reading from dir fails.
   **/
  async loadFormat(dir = this.dir) {
    const format = new Format();
  
    for await (const mode of modes(dir)) {
      format.add(mode)
    }

    return format;
  }

  get version() {
    return "1.0.1";
  }
};

/**
 * Configuration of ruted-format.
 **/
const Configuration = new _Configuration();

export {
  Configuration
};
