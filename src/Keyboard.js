/**
 * ruted-format: The rich Unicode text editing format
 *
 * Copyright © 2020 Huub de Beer <huub@campinecomputing.eu>
 *
 * ruted-format is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * ruted-format is distributed in the hope it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ruted-format. If not, see <https://www.gnu.org/licenses/>.
 **/

/**
 * @private
 **/
const QUERTY = ["`", "~", "1", "!", "2", "@", "3", "#", "4", "$", "5", "%",
  "6", "^", "7", "&", "8", "*", "9", "(", "0", ")", "-", "_", "=", "+", "q",
  "Q", "w", "W", "e", "E", "r", "R", "t", "T", "y", "Y", "u", "U", "i", "I",
  "o", "O", "p", "P", "[", "{", "]", "}", "\\", "|", "a", "A", "s", "S", "d",
  "D", "f", "F", "g", "G", "h", "H", "j", "J", "k", "K", "l", "L", ";", ":",
  "'", "\"", "z", "Z", "x", "X", "c", "C", "v", "V", "b", "B", "n", "N", "m",
  "M", ",", "<", ".", ">", "/", "?", " ", "\t"];

/**
 * Keyboard class representing a model of typeable characters.
 **/
const _Keyboard = class {
  #layout = QUERTY

  /**
   * Create a new Keyboard model.
   *
   * @param {Array} layout - The layout of this keyboard.
   **/
  constructor(layout) {
    this.#layout = layout;
  }

  /**
   * Can a string be typed using this keyboard?
   *
   * @param {String} str - The string to check.
   * @returns {Boolean} If each character in str can be typed by this
   * keyboard, return true.
   **/
  canBeTyped(str) {
    for (const index in [...str]) {
      const ch = str[index];
      if (!this.#layout.includes(ch)) {
        return false;
      }
    }
    return true;
  }

};

const Keyboard = new _Keyboard(QUERTY);

export {
  Keyboard
};
