/**
 * ruted-format: The rich Unicode text editing format
 *
 * Copyright © 2020 Huub de Beer <huub@campinecomputing.eu>
 *
 * ruted-format is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * ruted-format is distributed in the hope it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ruted-format. If not, see <https://www.gnu.org/licenses/>.
 **/
import {Keyboard} from "./Keyboard.js";
import {RutedError} from "./RutedError.js";

// Multi-byte Unicode characters are treated as multiple characters in a
// String. However, when converted to an Array, they are treated as single
// characters.
const str2UArr = (str) => [...str];
const uArr2Str = (uStr) => uStr.join("");

/**
 * Base class for replacement rule errors thrown when adding replacement rules
 * to a converter.
 *
 * @extends Error
 **/
const ReplacementRuleError = class extends RutedError {
  #source
  #symbol

  /**
   * Create a new ReplacementRuleError.
   *
   * @param {String} source - The source of the rule that resulted in an error
   * @param {String} symbol - The symbol of the rule that resulted in an error
   * @param {String} msg - The error message
   **/
  constructor(source, symbol, msg) {
    super(msg);
    this.#source = source;
    this.#symbol = symbol;
  }

  /**
   * The source of the erroneous rule.
   * 
   * @return {String}
   **/
  get source() {
    return this.#source;
  }

  /**
   * The symbol of the erroneous rule.
   *
   * @return {String}
   **/
  get symbol() {
    return this.#symbol;
  }
};

/**
 * Replacement rule error thrown when the source of a replacement rule is
 * the empty string.
 *
 * @extends ReplacementRuleError
 **/
const EmptySourceReplacementRuleError = class extends ReplacementRuleError {
  constructor(source, symbol) {
    super(source, symbol,
      `Expected a source string of at least 1 character; Found "${source}" instead.`
    );
  }
};

/**
 * Replacement rule error thrown when the source of a replacement rule
 * contains characters that are not directly available on the (querty) keyboard.
 *
 * @extends ReplacementRuleError
 **/
const UntypeableSourceReplacementRuleError = class extends ReplacementRuleError {
  constructor(source, symbol) {
    super(source, symbol,
      `Expected a source string that can be entered by keyboard; Found "${source}" instead.`
    );
  }
};

/**
 * Replacement rule error thrown when the symbol of a replacement rule is
 * either the empty string or consists of more than one Unicode character.
 *
 * @extends ReplacementRuleError
 **/
const NonSingletonSymbolReplacementRuleError = class extends ReplacementRuleError {
  constructor(source, symbol) {
    super(source, symbol,
      `Expected Unicode symbol to be exactly 1 character; Found "${symbol}" instead, which has ${symbol.length} characters.`
    );
  }
};

/**
 * Replacement rule error thrown when the source of a replacement rule already
 * exists in the converter you try to add the new rule to.
 *
 * @extends ReplacementRuleError
 **/
const DuplicateReplacementRuleError = class extends ReplacementRuleError {
  #existingRuleSymbol

  /**
   * Create a new ReplacementRuleError.
   *
   * @param {String} source - The source of the rule that resulted in an error
   * @param {String} symbol - The symbol of the rule that resulted in an error
   * @param {String} existingRuleSymbol - The already existing rule's symbol
   **/
  constructor(source, symbol, existingRuleSymbol) {
    super(source, symbol,
      `There already exists a rule for "${source}" (${existingRuleSymbol}); Skipping new rule (${source}, ${symbol}).`
    );
    this.#existingRuleSymbol = existingRuleSymbol;
  }

  /**
   * The existing rule's symbol.
   *
   * @return {String}
   **/
  get existingRuleSymbol() {
    return this.#existingRuleSymbol;
  }
};

/**
 * Representation of a non-matching final state.
 * @private
 **/
const NO_MATCH = Symbol("NO MATCH");

/**
 * State in a state machine to recognize replacement rule source strings in a text.
 *
 * @private
 **/
const State = class {
  #accepting = new Map()
  #transitions = new Map()

  /**
   * Construct a path in a DFA to recognize source for replacement with
   * symbol.
   *
   * @param {String} source - The source string to construct a path for
   * @param {String} symbol - The Unicode symbol to replace the source with
   * when recognized.
   * @param {Number} [index = 0] - The index of the character in the source
   * to create a sub path for. 0 ≤ index < source.length.
   *
   * @throw DuplicateReplacementRuleError Thrown when the source string already exists
   * in the DFA.
   **/
  set(source, symbol, index = 0) {
    if (index < source.length) {
      // Not yet reached an accepting state.
      const nextChar = source.slice(index, index + 1);

      if (!this.#transitions.has(nextChar)) {
        this.#transitions.set(nextChar, new State());
      }

      this.#transitions.get(nextChar).set(source, symbol, index + 1);
    } else {
      // Reached an accepting state.

      if (this.#accepting.has(source)) {
        throw new DuplicateReplacementRuleError(source, symbol, this.#accepting.get(source));
      }

      // Mark this state as accepting for source.
      this.#accepting.set(source, symbol);
    }
  }

  /**
   * Get symbol of a recognized source string in the input string, if any, and
   * return both source and symbol. If no replacement rule source is
   * recognized, the symbol is {@link NO_MATCH}.
   *
   * @param {String[]} uStr - The input String as an array of singleton Unicode
   * characters.
   * @param {Number} start - The start index to start looking in uStr. 0 ≤
   * start < uStr.length
   * @param {Number} len - The length of the source String recognized so far.
   * 0 ≤ len < uStr.length - start
   *
   * @return {{source: String, symbol: String | NO_MATCH}} The recognized source and symbol 
   **/
  get(uStr, start, len = 0) {
    const end = start + len;
    const nextChar = uArr2Str(uStr.slice(end, end + 1));

    if (this.#transitions.has(nextChar)) {
      return this.#transitions.get(nextChar).get(uStr, start, len + 1);
    } else {
      const source = uArr2Str(uStr.slice(start, end));
      const symbol = this.#accepting.get(source) || NO_MATCH;

      return {symbol, source};
    }
  }
};

const checkReplacementRule = (source, symbol) => {
  if (0 >= source.length) {
    throw new EmptySourceReplacementRuleError(source, symbol);
  }

  if (!Keyboard.canBeTyped(source)) {
    // The source should be in "keyboard" range. If it is not, it cannot be
    // represented by the keyboard easily
    throw new UntypeableSourceReplacementRuleError(source, symbol);
  }

  // Multi-byte Unicode symbol strings have a length >= 1, while still being a
  // single symbol. To circumvent this issue, the string is converted to an
  // array first before checking the length.
  if (1 !== str2UArr(symbol).length) {
    throw new NonSingletonSymbolReplacementRuleError(source, symbol);
  }

  return true;
};

/**
 * A replacement rule consists of a source string and the Unicode symbol that should replace
 * that source string. A ReplacementRule is a subtype of a {@link Rule}; A
 * list of rules can be used to construct a new converter.
 *
 * @typedef {Object} ReplacementRule
 * @property {String} ReplacementRule.source - The source string to be
 * replaced by this replacement rule.
 * @property {String} ReplacementRule.symbol - The Unicode symbol to replace
 * the source string with.
 **/

/**
 * A Converter converts strings with source representations of Unicode symbols
 * to the same string with those source representations replaced by the
 * Unicode symbols they represent. 
 **/
const Converter = class {
  #lookup = new State()

  /**
   * Create a new Converter based on the set of replacement rules.
   *
   * @param {ReplacementRule[]} [replacementRules = []] - a list of replacement rules.
   * Each rule consists of a singleton Unicode symbol and its source
   * representation. Both source and symbol have type String. This parameter
   * is optional and defaults to the empty array. A converter without any
   * replacement rules acts as the identity converter.
   *
   * @throws See {@link Converter#addReplacementRule} for errors thrown when
   * adding each of the replacement rules.
   **/
  constructor(replacementRules = []) {
    replacementRules.forEach(({source, symbol}) => this.addReplacementRule(source, symbol));
  }

  /**
   * Add a replacement rule to this converter:
   *
   * @param {String} source - The source string to replace. The source string
   * need to be unique in a converter, should only contain "typeable"
   * characters and be at least one character long.
   * @param {String} symbol - The Unicode symbol to replace the source with.
   * The symbol is exactly one character. A Unicode symbol can occur more than
   * once in a converter.
   *
   * @throws {DuplicateReplacementRuleError} A source can occur only once in a
   * converter.
   * @throws {EmptySourceReplacementRuleError} A source cannot be empty.
   * @throws {UntypeableSourceReplacementRuleError} A source can only contain
   * "typeable" characters.
   * @throws {NonSingletonSymbolReplacementRuleError} A symbol should be
   * exactly one Unicode character.
   **/
  addReplacementRule(source, symbol) {
    if (checkReplacementRule(source, symbol)) {
      this.#lookup.set(source, symbol);
    }
  }

  /**
   * Convert the input string to an output string following the replacement
   * rules of this converter.
   *
   * @param {String} input - The input string to convert.
   * @returns {String} The input string with all replacement rules applied.
   **/
  convert(input) {
    const inputArr = str2UArr(input);
    let start = 0;
    let outputArr = [];

    while (start < inputArr.length) {
      const {source, symbol} = this.#lookup.get(inputArr, start);

      if (symbol === NO_MATCH) {
        // No match: Add the current character under investigation to the
        // output.
        outputArr.push(inputArr[start]);
        start++;
      } else {
        // There is a match: Add the found symbol to the output.
        outputArr.push(symbol);
        start += source.length;
      }
    }

    return uArr2Str(outputArr);
  }

};

export {
  Converter,
  ReplacementRuleError,
  DuplicateReplacementRuleError,
  EmptySourceReplacementRuleError,
  NonSingletonSymbolReplacementRuleError,
  UntypeableSourceReplacementRuleError  
};
