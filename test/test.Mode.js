import chai from "chai";
import {
  Mode,
  EmptyNameRuleModeError,
  EmptyDocumentationFieldModeError,
  NonSingletonKeyModeError,
  NonTypeableKeyModeError,
  UnknownRuleModeError
} from "../src/Mode.js";
import {ReplacementRuleError} from "../src/Converter.js";

const expect = chai.expect;

const RULES = [
  {
    name: "smaller or equal",
    source: "<=",
    symbol: "≤"
  },
  {
    name: "left double arrow",
    source: "<==",
    symbol: "⇐"
  },
  {
    name: "greater or equal",
    source: ">=",
    symbol: "≥"
  },
  {
    name: "equivalent",
    source: "===",
    symbol: "≡"
  },
  {
    name: "pile of poo",
    source: "BLERGH",
    symbol: "💩"
  }
];

describe("Mode", function () {
  const key = "a";
  const name = "a mode";
  const description = "this is a mode";

  const ruleName = "right arrow";
  const ruleSource = "->";
  const ruleSymbol = "→";
    
  const modeJSON = '{"key":"a","name":"a mode","description":"this is a mode","rules":[{"name":"right arrow","source":"->","symbol":"→"}]}';

  describe("Mode.fromJSON", function () {
    it("should create a mode from a JSON string representation of a mode", function () {
      const m = Mode.fromJSON(modeJSON);
      expect(m.key).to.equal(key);
      expect(m.name).to.equal(name);
      expect(m.description).to.equal(description);
      expect([...m.rules()][0].name).to.equal(ruleName);
      expect([...m.rules()][0].source).to.equal(ruleSource);
      expect([...m.rules()][0].symbol).to.equal(ruleSymbol);      
    });

    it("should throw a EmptyDocumentationFieldModeError when the key, name, or description is empty", function () {
      const emptyKeyErrorModeJSON = '{"key":"","name":"a mode","description":"this is a mode","rules":[{"name":"right arrow","source":"->","symbol":"→"}]}';
      const noKeyErrorModeJSON = '{"name":"a mode","description":"this is a mode","rules":[{"name":"right arrow","source":"->","symbol":"→"}]}';
      const emptyNameErrorModeJSON = '{"key":"a","name":"","description":"this is a mode","rules":[{"name":"right arrow","source":"->","symbol":"→"}]}';
      const noNameErrorModeJSON = '{"key":"a","description":"this is a mode","rules":[{"name":"right arrow","source":"->","symbol":"→"}]}';
      const emptyDescriptionErrorModeJSON = '{"key":"a","name":"a mode","description":"","rules":[{"name":"right arrow","source":"->","symbol":"→"}]}';
      const noDescriptionErrorModeJSON = '{"key":"a","name":"a mode","rules":[{"name":"right arrow","source":"->","symbol":"→"}]}';

      expect(() => Mode.fromJSON(emptyKeyErrorModeJSON)).to.throw(EmptyDocumentationFieldModeError);
      expect(() => Mode.fromJSON(emptyNameErrorModeJSON)).to.throw(EmptyDocumentationFieldModeError);
      expect(() => Mode.fromJSON(emptyDescriptionErrorModeJSON)).to.throw(EmptyDocumentationFieldModeError);

      expect(() => Mode.fromJSON(noKeyErrorModeJSON)).to.throw(EmptyDocumentationFieldModeError);
      expect(() => Mode.fromJSON(noNameErrorModeJSON)).to.throw(EmptyDocumentationFieldModeError);
      expect(() => Mode.fromJSON(noDescriptionErrorModeJSON)).to.throw(EmptyDocumentationFieldModeError);
    });

    it("should throw NonSingletonKeyModeError when the key is longer than one character", function () {
      const twoLetterKeyModeJSON = '{"key":"12","name":"a mode","description":"this is a mode","rules":[{"name":"right arrow","source":"->","symbol":"→"}]}';
      expect(() => Mode.fromJSON(twoLetterKeyModeJSON)).to.throw(NonSingletonKeyModeError);
    });

    it("should throw NonTypeableKeyModeError when the key is a 'non-typeable' character like '⇐'", function () {
      const nonTypeableKeyModeError = '{"key":"⇐","name":"a mode","description":"this is a mode","rules":[{"name":"right arrow","source":"->","symbol":"→"}]}';
      expect(() => Mode.fromJSON(nonTypeableKeyModeError)).to.throw(NonTypeableKeyModeError);
    });

    it("should throw EmptyNameRuleModeError when the rule's name is empty", function () {
      const emptyRuleNameModeErrorJSON = '{"key":"a","name":"a mode","description":"this is a mode","rules":[{"name":"","source":"->","symbol":"→"}]}';
      expect(() => Mode.fromJSON(emptyRuleNameModeErrorJSON)).to.throw(EmptyNameRuleModeError);
    });

    it("should throw ReplacementRuleError when an invalid rule is added to the mode", function () {
      const invalidRuleModeErrorJSON = '{"key":"a","name":"a mode","description":"this is a mode","rules":[{"name":"right arrow","source":"","symbol":"→"}]}';
      expect(() => Mode.fromJSON(invalidRuleModeErrorJSON)).to.throw(ReplacementRuleError);
    });
  });

  describe("constructor", function () {
    const multiLineDescription = "This is a mode. Its description has\nmultiple lines.";

    it("should create a mode", function () {
      const m = new Mode(key, name, description);
      expect(m.key).to.equal(key);
      expect(m.name).to.equal(name);
      expect(m.description).to.equal(description);
    });

    it("should create a mode with a multi-line description", function () {
      const m = new Mode(key, name, multiLineDescription);
      expect(m.key).to.equal(key);
      expect(m.name).to.equal(name);
      expect(m.description).to.equal(multiLineDescription);
    });

    it("should throw EmptyDocumentationFieldModeError when any of the key, name, or description is empty", function () {
      expect(() => new Mode("", name, description)).to.throw(EmptyDocumentationFieldModeError);
      expect(() => new Mode(key, "", description)).to.throw(EmptyDocumentationFieldModeError);
      expect(() => new Mode(key, name, "")).to.throw(EmptyDocumentationFieldModeError);
    });

    it("should throw NonSingletonKeyModeError when the key is longer than one character", function () {
      expect(() => new Mode("12", name, description)).to.throw(NonSingletonKeyModeError);
    });

    it("should throw NonTypeableKeyModeError when the key is a 'non-typeable' character like '⇐'", function () {
      expect(() => new Mode("⇐", name, description)).to.throw(NonTypeableKeyModeError);
    });
  });

  describe("description=", function () {
    it("should change the description", function () {
      const newDescription = "This is a new description";
      const m = new Mode(key, name, description);
      expect(m.description).to.equal(description);
      m.description = newDescription;
      expect(m.description).to.equal(newDescription);
    });

    it("should throw EmptyDocumentationFieldModeError when the new description is empty", function () {
      const m = new Mode(key, name, description);
      expect(m.description).to.equal(description);
      expect(() => m.description = "").to.throw(EmptyDocumentationFieldModeError);
      expect(m.description).to.equal(description);
    });
  });

  describe("*rules", function () {
    it("should yield zero times for a mode with no replacement rules", function () {
      const m = new Mode(key, name, description);
      expect([...m.rules()].length).to.equal(0);
    });

    it("should yield a value exactly as many times as there are replacment rules in the mode", function () {
      const m = new Mode(key, name, description);
      for (const rule of RULES) {
        m.addRule(rule.name, rule.source, rule.symbol);
      }
      expect([...m.rules()].length).to.equal(RULES.length);

      let index = 0;
      for (const rule of m.rules()) {
        expect(rule.name).to.equal(RULES[index].name);
        index++;
      }
    });
  });

  describe("addRule", function () {

    it("should add a new rule", function () {
      const m = new Mode(key, name, description);
      expect([...m.rules()].length).to.equal(0);
      m.addRule(ruleName, ruleSource, ruleSymbol);
      const rules = [...m.rules()];
      expect(rules.length).to.equal(1);
      expect(rules[0].name).to.equal(ruleName);
      expect(rules[0].source).to.equal(ruleSource);
      expect(rules[0].symbol).to.equal(ruleSymbol);
      expect(m.convert(ruleSource)).to.equal(ruleSymbol);
    });

    it("should throw EmptyNameRuleModeError when the rule's name is empty", function () {
      const m = new Mode(key, name, description);
      expect(() => m.addRule("", ruleSource, ruleSymbol)).to.throw(EmptyNameRuleModeError);
    });

    it("should throw ReplacementRuleError when an invalid rule is added to the mode", function () {
      const m = new Mode(key, name, description);
      expect(() => m.addRule(ruleName, "", ruleSymbol)).to.throw(ReplacementRuleError);
    });
  });

  describe("removeRule", function () {
    it("should remove a rule if the rule is in the mode", function () {
      const m = new Mode(key, name, description);
      m.addRule(ruleName, ruleSource, ruleSymbol);
      expect([...m.rules()].length).to.equal(1);
      expect(m.convert(ruleSource)).to.equal(ruleSymbol);
      m.removeRule({name: ruleName, source: ruleSource, symbol: ruleSymbol});
      expect([...m.rules()].length).to.equal(0);
      expect(m.convert(ruleSource)).to.equal(ruleSource);
    });

    it("should not remove a rule if the rule is not in the mode", function () {
      const m = new Mode(key, name, description);
      m.addRule(ruleName, ruleSource, ruleSymbol);
      expect([...m.rules()].length).to.equal(1);
      expect(m.convert(ruleSource)).to.equal(ruleSymbol);
      m.removeRule({name: "some other name makes for a different rule", source: ruleSource, symbol: ruleSymbol});
      expect([...m.rules()].length).to.equal(1);
      expect(m.convert(ruleSource)).to.equal(ruleSymbol);
    });
  });

  describe("updateRuleName", function () {
    it("should update a rule's name", function () {
      const newName = "new name";
      const m = new Mode(key, name, description);
      m.addRule(ruleName, ruleSource, ruleSymbol);
      expect([...m.rules()][0].name).to.equal(ruleName);
      m.updateRuleName({
        name: ruleName, 
        source: ruleSource,
        symbol: ruleSymbol
      }, newName);
      expect([...m.rules()][0].name).to.equal(newName);
    });

    it("should throw EmptyNameRuleModeError when the new name is empty", function () {
      const m = new Mode(key, name, description);
      m.addRule(ruleName, ruleSource, ruleSymbol);
      expect([...m.rules()][0].name).to.equal(ruleName);
      const errorF = () => m.updateRuleName({
        name: ruleName, 
        source: ruleSource,
        symbol: ruleSymbol
      }, "");
      expect(errorF).to.throw(EmptyNameRuleModeError);
    });

    it("should throw UnknownRuleModeError when the rule to update cannot be found in this mode", function () {
      const m = new Mode(key, name, description);
      m.addRule(ruleName, ruleSource, ruleSymbol);
      expect([...m.rules()][0].name).to.equal(ruleName);
      const errorF = () => m.updateRuleName({
        name: "some other name makes this a different rule", 
        source: ruleSource,
        symbol: ruleSymbol
      }, "another name");
      expect(errorF).to.throw(UnknownRuleModeError);
    });
  });

  describe("convert", function () {
    const m = new Mode(key, name, description);
    for (const rule of RULES) {
      m.addRule(rule.name, rule.source, rule.symbol);
    }

    it("should convert a string according to the replacement rules (see also Converter.convert)", function () {
      expect(m.convert("hello world! 3 < 4 and 5 == 5")).to.be.equal("hello world! 3 < 4 and 5 == 5");
      expect(m.convert("<=")).to.be.equal("≤");
      expect(m.convert("<<=")).to.be.equal("<≤");
      expect(m.convert(" <= <= ")).to.be.equal(" ≤ ≤ ");
    });
  });

  describe("toJSON", function () {
    it("should represent a mode as a JSON string", function () {
      const m = new Mode(key, name, description);
      m.addRule(ruleName, ruleSource, ruleSymbol);
      const json = m.toJSON();
      expect(json).to.equal(modeJSON);
    });

    it("should generate a JSON string that can be used to create a new Mode with the exact same fields and rules", function () {
      const m = new Mode(key, name, description);
      m.addRule(ruleName, ruleSource, ruleSymbol);
      const json = m.toJSON();
      const mPrime = Mode.fromJSON(json);
      expect(mPrime.key).to.equal(m.key);
      expect(mPrime.name).to.equal(m.name);
      expect(mPrime.description).to.equal(m.description);
      expect(mPrime.toJSON()).to.equal(m.toJSON());
      expect([...mPrime.rules()][0].name).to.equal([...m.rules()][0].name);
      expect([...mPrime.rules()][0].source).to.equal([...m.rules()][0].source);
      expect([...mPrime.rules()][0].symbol).to.equal([...m.rules()][0].symbol);      
    });
  });
});
