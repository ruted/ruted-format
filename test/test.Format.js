import chai from "chai";
import {
  Format,
  DuplicateModeFormatError,
  IllegalModeKeyFormatError,
  ModeNotFoundFormatError
} from "../src/Format.js";
import {
  Mode
} from "../src/Mode.js";

const expect = chai.expect;

describe("Format", function () {
  const mode = Mode.fromJSON('{"key":"a","name":"a mode","description":"this is a mode","rules":[{"name":"right arrow","source":"->","symbol":"→"}]}');
  const duplicateMode = Mode.fromJSON('{"key":"a","name":"a mode","description":"this is a mode","rules":[{"name":"right arrow","source":"->","symbol":"→"}]}');
  const anotherMode = Mode.fromJSON('{"key":"b","name":"a mode","description":"this is a mode","rules":[{"name":"right arrow","source":"->","symbol":"→"}]}');

  describe("constructor", function () {
    it("should create a new Format", function () {
      const f = new Format();
      expect([...f.modes()].length).to.equal(0);
    });
  });

  describe("*modes", function () {
    it("should yield zero times for a format with no modes", function () {
      const f = new Format();
      expect([...f.modes()].length).to.equal(0);
    });

    it("should yield a value exactly as many times as there are modes in a format", function () {
      const f = new Format();
      expect([...f.modes()].length).to.equal(0);
      f.add(mode);
      expect([...f.modes()].length).to.equal(1);
      f.add(anotherMode);
      expect([...f.modes()].length).to.equal(2);
    });
  });

  describe("add", function () {
    it("should add a mode to a format", function () {
      const f = new Format();
      expect([...f.modes()].length).to.equal(0);
      f.add(mode);
      expect([...f.modes()].length).to.equal(1);
      expect(f.get(mode.key).name).to.equal(mode.name);
    });

    it("should throw an error when trying to add another mode with the same key", function () {
      const f = new Format();
      f.add(mode);
      expect(() => f.add(duplicateMode)).to.throw(DuplicateModeFormatError);
    });
  });

  describe("get", function () {
    it("should get the mode specified by a key", function () {
      const f = new Format();
      f.add(mode);
      expect(f.get(mode.key)).to.equal(mode);
      f.add(anotherMode);
      expect(f.get(anotherMode.key)).to.equal(anotherMode);
      expect(f.get(mode.key)).to.equal(mode);
    });

    it("should throw an error when the specified key is invalid", function () {
      const f = new Format();
      expect(() => f.get("Hello")).to.throw(IllegalModeKeyFormatError);
      expect(() => f.get("±")).to.throw(IllegalModeKeyFormatError);
    });

    it("should throw an error when there is no mode with a key equal to the specified key", function () {
      const f = new Format();
      expect(() => f.get("?")).to.throw(ModeNotFoundFormatError);
    });
  });

  describe("has", function () {
    it("should return true when there is a mode in this format with the specified key", function () {
      const f = new Format();
      f.add(mode);
      expect(f.has(mode.key)).to.be.true;
    });

    it("should return false when there is no mode in this format with the specified key", function () {
      const f = new Format();
      f.add(mode);
      expect(f.has("?")).to.be.false;
    });

    it("should throw an error when the specified key is invalid", function () {
      const f = new Format();
      expect(() => f.has("Hello")).to.throw(IllegalModeKeyFormatError);
      expect(() => f.has("±")).to.throw(IllegalModeKeyFormatError);
    });
  });

  describe("remove", function () {
    it("should remove the mode with the specified key", function () {
      const f = new Format();
      f.add(mode);
      expect([...f.modes()].length).to.equal(1);
      f.remove(mode.key);
      expect([...f.modes()].length).to.equal(0);
    });

    it("should throw an error when there is no mode with a key equal to the specified key", function () {
      const f = new Format();
      expect(() => f.remove("?")).to.throw(ModeNotFoundFormatError);
    });

    it("should throw an error when the specified key is invalid", function () {
      const f = new Format();
      expect(() => f.remove("Hello")).to.throw(IllegalModeKeyFormatError);
      expect(() => f.remove("±")).to.throw(IllegalModeKeyFormatError);
    });
  });

});
