import chai from "chai";
import {
  Converter, 
  DuplicateReplacementRuleError,
  EmptySourceReplacementRuleError,
  NonSingletonSymbolReplacementRuleError,
  UntypeableSourceReplacementRuleError
} from "../src/Converter.js";

const expect = chai.expect;

const RULES = [
  {
    source: "<=",
    symbol: "≤"
  },
  {
    source: "<==",
    symbol: "⇐"
  },
  {
    source: ">=",
    symbol: "≥"
  },
  {
    source: "===",
    symbol: "≡"
  },
  {
    source: "BLERGH",
    symbol: "💩"
  }
];

describe("Converter", function () {
  describe("constructor", function () {
    it("should create the identity converter with an empty list of replacement rules", function () {
      const c = new Converter([]);
      for (const s in ["", "h", "hello world!", "💩", "blergh:💩!"]) {
        expect(c.convert(s)).to.be.equal(s);
      }
    });

    it("should create a converter with rules", function () {
      const c = new Converter(RULES);
      expect(c.convert("")).to.be.equal("");
      expect(c.convert("hello")).to.be.equal("hello");
      expect(c.convert("<=")).to.be.equal("≤");
      expect(c.convert("<==")).to.be.equal("⇐");
      expect(c.convert("<= <== >= === BLERGH")).to.be.equal("≤ ⇐ ≥ ≡ 💩");
    });
  });

  describe("addReplacementRule", function () {
    it("should add a rule to the converter", function () {
      const c = new Converter([]);
      expect(c.convert("<=")).to.be.equal("<=");
      c.addReplacementRule("<=", "≤");
      expect(c.convert("<=")).to.be.equal("≤");
    });

    it("should throw EmptySourceReplacementRuleError when adding a replacement rule with an empty source", function () {
      const c = new Converter([]);
      const errorF = () => c.addReplacementRule("", "⇐");
      expect(errorF).to.throw(EmptySourceReplacementRuleError);
    });

    it("should throw UntypeableSourceReplacementRuleError when adding a replacement rule with a source with 'nontypeable' characters like ≤ in it", function () {
      const c = new Converter([]);
      const errorF = () => c.addReplacementRule("≤|", "⊲");
      expect(errorF).to.throw(UntypeableSourceReplacementRuleError);
    });

    it("should throw DuplicateReplacementRuleError when adding another replacement rule for the same source", function () {
      const c = new Converter([]);
      c.addReplacementRule("<=", "≤");
      const errorF = () => c.addReplacementRule("<=", "⇐");
      expect(errorF).to.throw(DuplicateReplacementRuleError);
    });

    it("should throw NonSingletonSymbolReplacementRuleError when adding a rule with a symbol that contains more than one character", function () {
      const c = new Converter([]);
      const errorF = () => c.addReplacementRule("<=", "≤2");
      expect(errorF).to.throw(NonSingletonSymbolReplacementRuleError);
    });
    
    it("should throw NonSingletonSymbolReplacementRuleError when adding a rule with a symbol that contains less than one character", function () {
      const c = new Converter([]);
      const errorF = () => c.addReplacementRule("<=", "");
      expect(errorF).to.throw(NonSingletonSymbolReplacementRuleError);
    });
  });

  describe("convert", function () {
    const c = new Converter(RULES);

    it("should convert all occurences of '<=' by '≤'", function () {
      expect(c.convert("hello world! 3 < 4 and 5 == 5")).to.be.equal("hello world! 3 < 4 and 5 == 5");
      expect(c.convert("<=")).to.be.equal("≤");
      expect(c.convert("<<=")).to.be.equal("<≤");
      expect(c.convert(" <= <= ")).to.be.equal(" ≤ ≤ ");
    });

    it("should convert the longest possible source, i.e., convert '<==' rather than '<='", function () {
      expect(c.convert("<==")).to.be.equal("⇐");
    });

    it("should convert to multibyte Unicode symbols", function () {
      expect(c.convert("BLERGH")).to.be.equal("💩");
      expect(c.convert("💩BLERGH💩")).to.be.equal("💩💩💩");
    });
  });
});
