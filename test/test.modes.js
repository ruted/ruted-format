import fs from "fs";
import path from "path";
import chai from "chai";
import {Configuration} from "../src/Configuration.js";

const expect = chai.expect;
const resolve = path.resolve;

const SRC_DIR = resolve("");
const MODES_DIR = resolve(SRC_DIR, "modes");

async function loadFormatFromDir(dir) {
  try {
    await Configuration.loadFormat(dir);
  } catch (err) {
    throw new Error(`Something went wrong testing the default format (loaded from "${dir}": `, err);
  }
};

describe("Default format", function () {
  it("should be loaded without errors", function () {
    expect(async function() {await loadFormatFromDir(MODES_DIR);}).to.not.throw();
  });
});
